package parse_json;

public class catfacts_bean{

    // declare the attributes as Strings
    private String fact;
    private Integer length;

    // methods for setting the Strings - GETTERS & SETTERS

    // GETTER
    public String getFact() {
        return fact;
    }
    
    // SETTER
    public void setFact(String fact){
        this.fact = fact;
    }

    // GETTER
    public Integer getLength(){
        return length;
    }

    // SETTER
    public void setLength(Integer length){
        this.length = length;
    }
}
