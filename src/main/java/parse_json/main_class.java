package parse_json;

import java.io.IOException;
import java.net.http.HttpResponse;

public class main_class{

    public static void main(String[] args) throws IOException, InterruptedException {

        callout_class callout_instance = new callout_class();
        HttpResponse<String> response = callout_instance.new_callout();

        // print the status code
        System.out.println(response.statusCode());

        // print the body of the response
        System.out.println(response.body());

        String json_attribute = "{ \"country\" : \"United Kingdom\" }";

        // Instantiate the parse_JSON class
        parse_JSON parse_JSON_instance = new parse_JSON();
        //parse_JSON_instance.jsonNode_method(response, json_attribute);


    }


}