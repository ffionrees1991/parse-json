package parse_json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;



// my goal here is to be able to instantiate this class and call the methods, to parse the response.body and get certain parts of the JSON. We have created a different branch in case this goes wrong!!

public class parse_JSON{

    // example JSON string
    String json_body;

    // the part of the JSON string that we want
    String json_attribute = "{ \"country\" : \"United Kingdom\" }";

    // method to instantiate an ObjectMapper
    public ObjectMapper newObjectMapper(){

        ObjectMapper defaultObjectMapper = new ObjectMapper();

        return defaultObjectMapper;
    }

    // instantiate an ObjectMapper using the method
    ObjectMapper defaultObjectMapper = new ObjectMapper();


    // method to deserialize. Takes as parameters 1. the response.body to read, and 2. the particular part of the JSON to read
    public JsonNode jsonNode_method(String json_body, String json_attribute) throws JsonMappingException, JsonProcessingException{

        JsonNode jsonNode = defaultObjectMapper.readTree(json_body);
        System.out.println(jsonNode.get(json_attribute).asText());

        return jsonNode;
    }
 
    // the JSON data is an array of countries with attributes denoted as above


}


